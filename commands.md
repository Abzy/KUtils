# Commands

## Key
| Symbol     | Meaning                    |
| ---------- | -------------------------- |
| (Argument) | This argument is optional. |

## Data
| Commands | Arguments | Description                                                                           |
| -------- | --------- | ------------------------------------------------------------------------------------- |
| DataSave | <none>    | This command lets you modify a Data object's contents.                                |
| DataSee  | <none>    | This command demonstrates loading and injecting Data objects by viewing its contents. |

## ServicesDemo
| Commands     | Arguments | Description              |
| ------------ | --------- | ------------------------ |
| DependsOnAll | <none>    | I depend on all services |

## Misc
| Commands    | Arguments | Description             |
| ----------- | --------- | ----------------------- |
| SomeCommand | <none>    | No Description Provider |

## Utility
| Commands     | Arguments          | Description                                    |
| ------------ | ------------------ | ---------------------------------------------- |
| Add          | Integer, Integer   | Add two numbers together                       |
| Conversation | <none>             | Test the implementation of the ConversationDSL |
| Echo         | Text               | No Description Provider                        |
| Embed        | <none>             | Display an example embed.                      |
| File         | File               | Input a file and display its name.             |
| Help         | (Command)          | Display a help menu.                           |
| Menu         | <none>             | Display an example menu.                       |
| NumberOrWord | Integer \| Word    | Enter a word or a number                       |
| OptionalAdd  | Integer, (Integer) | Add two numbers together                       |
| Version, V   | <none>             | A command which will show the version.         |

