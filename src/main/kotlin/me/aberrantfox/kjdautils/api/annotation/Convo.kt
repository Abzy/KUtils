package me.aberrantfox.kjdautils.api.annotation

@Target(AnnotationTarget.FUNCTION)
annotation class Convo