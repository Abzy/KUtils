package me.aberrantfox.kjdautils.api.annotation

annotation class Data(val path: String, val killIfGenerated: Boolean = true)